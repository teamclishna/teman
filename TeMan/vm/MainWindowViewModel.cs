﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using clUtils.gui;
using clUtils.util;

using TeMan.commands;
using TeMan.model;
using TeMan.utils;

namespace TeMan.vm
{
    /// <summary>
    /// メインウィンドウのViewModel
    /// </summary>
    public class MainWindowViewModel : NotifiableViewModel
    {
        #region Commands

        public RelayCommand FileOpenCommand { get; set; }
        public RelayCommand FileSaveCommand { get; set; }
        public RelayCommand FileSaveAsCommand { get; set; }
        public RelayCommand ExitCommand { get; set; }

        public RelayCommand CommandHelpCommand { get; set; }
        public RelayCommand ExecEditCommand { get; set; }

        #endregion

        #region Fields

        private Document _currentDocument;
        private string _commandLine;
        private string _notifyMessage;

        #endregion

        /// <summary>
        /// このViewModelを格納するWindow
        /// </summary>
        public MainWindow Parent { get; set; }

        /// <summary>
        /// 編集コマンドを実行するためのコントローラ
        /// </summary>
        public CommandController _cmdController;

        /// <summary>
        /// 現在編集中のドキュメント
        /// </summary>
        public Document CurrentDocument {
            get { return this._currentDocument; }
            set
            {
                this._currentDocument = value;
                OnPropertyChanged("CurrentDocument");
                OnPropertyChanged("Snapshots");
                OnPropertyChanged("CurrentSnapshot");
                OnPropertyChanged("DisplayTitle");
            }
        }

        /// <summary>
        /// 現在編集中のドキュメント内に保持している編集履歴のスナップショット群
        /// </summary>
        public ObservableCollection<Snapshot> Snapshots
        {
            get
            {
                return (
                    CurrentDocument == null ? null :
                    CurrentDocument.Snapshots == null ? null :
                    CurrentDocument.Snapshots);
            }
        }

        /// <summary>
        /// 現在表示中のスナップショット
        /// </summary>
        public Snapshot CurrentSnapshot
        {
            get
            {
                return (
                    CurrentDocument == null ? null :
                    CurrentDocument.Snapshots == null ? null :
                    CurrentDocument.Current);
            }
        }

        /// <summary>
        /// ファイルを開くときのエンコーディング
        /// </summary>
        public System.Text.Encoding DefaultEncoding
        { get; set; }

        /// <summary>
        /// タブ文字のサイズ.
        /// タブの幅を調整するために tab-size スタイルシートを使っているが、IE は対応していないのでこのプロパティは機能しない.
        /// </summary>
        public int TabSize
        { get; set; }

        /// <summary>
        /// 画面入力されたコマンドライン
        /// </summary>
        public string CommandLine
        {
            get { return this._commandLine; }
            set
            {
                this._commandLine = value;
                OnPropertyChanged("CommandLine");
            }
        }

        /// <summary>
        /// 画面上に表示する1行通知メッセージ
        /// </summary>
        public string NotifyMessage
        {
            get { return this._notifyMessage; }
            set
            {
                this._notifyMessage = value;
                OnPropertyChanged("NotifyMessage");
            }
        }

        /// <summary>
        /// ウィンドウに表示するタイトル文字列.
        /// </summary>
        public string DisplayTitle
        { get { return (this.CurrentDocument != null ? this.CurrentDocument.OpenFile.Name + " - TeMan" : "TeMan"); } }

        /// <summary>
        /// ファイルを開くコマンド.
        /// 引数無しでopenコマンドを実行し、ダイアログを表示する.
        /// </summary>
        private void ExecFileOpen()
        { this._cmdController.ExecuteMetaCommand(this, "open", new string[0]); }

        /// <summary>
        /// ファイルを上書き保存するコマンド
        /// </summary>
        private void ExecFileSave()
        {
            this._cmdController.ExecuteMetaCommand(
                this,
                "save",
                new string[] { this.CurrentDocument.OpenFile.FullName }
                );
        }

        /// <summary>
        /// ファイルを「名前を付けて保存」するコマンド
        /// </summary>
        private void ExecFileSaveAs()
        {
            this._cmdController.ExecuteMetaCommand(
                this,
                "save",
                new string[0]
                );
        }

        /// <summary>
        /// ウィンドウを閉じてアプリケーションを終了するコマンド
        /// </summary>
        private void ExecFileExit()
        {
            this.Parent.Close();
        }

        /// <summary>
        /// 指定したファイルを開く.
        /// 
        /// </summary>
        /// <param name="files">オープンするファイルのパス</param>
        /// <param name="enc">ファイルのエンコーディング文字列. 指定しない場合はnull</param>
        public void Open(string[] files, string enc)
        {
            var argsList = new List<string>(files);
            if(enc != null)
            {
                argsList.Add("--encoding");
                argsList.Add(enc);
            }
            this._cmdController.ExecuteMetaCommand(this, "open", argsList.ToArray());
        }

        /// <summary>
        /// テキストの編集コマンドを入力したときの動作
        /// </summary>
        private void ExecEdit()
        {
            // コマンドラインに何も指定されていない場合は何もせずに終了
            if(string.IsNullOrEmpty(this.CommandLine))
            { return; }

            // 入力されたコマンドをパイプで分割して順に実行する
            var commands = CommandLinePerserUtil.Parse(this.CommandLine);
            try
            {
                var resultSnapshot = ExecCommand(commands, (this.CurrentDocument != null ? this.CurrentDocument.Current : null));

                // コマンドラインを消去、ツリービュー上のカレントアイテムを最終出力のスナップショットに変更する
                this.CommandLine = "";
                if (this.CurrentSnapshot != null)
                {
                    this.CurrentSnapshot.IsSelected = true;
                }
            }
            catch (CommandNotFoundException eNotFound)
            {
                this.NotifyMessage = string.Format(Properties.Resources.MSG_ERR_COMMAND_NOT_FOUND, eNotFound.CommandName);
            }
            catch (InvalidSwitchException eSW)
            {
                this.NotifyMessage = string.Format(Properties.Resources.MSG_ERR_INVALID_SWITCH, eSW.SwitchName);
            }
            catch (MissingParameterException eMissingParams)
            {
                var paramName = (
                    eMissingParams.Option.LongSwitch != null ? eMissingParams.Option.LongSwitch :
                    new string(new char[] { eMissingParams.Option.ShortSwitch }));
                this.NotifyMessage = string.Format(Properties.Resources.MSG_ERR_MISSING_PARAM, paramName);
            }
            catch (MultipleParametersException eMultiParam)
            {
                var paramName = (
                    eMultiParam.Option.LongSwitch != null ? eMultiParam.Option.LongSwitch :
                    new string(new char[] { eMultiParam.Option.ShortSwitch }));
                this.NotifyMessage = string.Format(Properties.Resources.MSG_ERR_MULTIPLE_PARAM, paramName);
            }
            catch (Exception eUnknown)
            {
                this.NotifyMessage = string.Format(eUnknown.Message);
            }
        }

        /// <summary>
        /// MetaCommand の実行を試みる
        /// </summary>
        /// <param name="args">実行するコマンドのパラメータ情報</param>
        /// <returns>
        /// 実際にコマンドが実行された場合は true.
        /// 該当するMetaCommandが存在しなかった場合は false.
        /// </returns>
        private bool ExecMetaCommand(CommandLineArgs args)
        {
            try
            {
                // コマンドが正常に実行出来たらここで終了
                // 該当するコマンドが無く、CommandNotFoundが返されたら false (該当コマンド無し) を返して終了.
                this._cmdController.ExecuteMetaCommand(
                    this,
                    args.CommandName,
                    args.Args.ToArray());

                this.CommandLine = "";
                return true;
            }
            catch (CommandNotFoundException)
            { return false; }
        }

        /// <summary>
        /// パースしたコマンドを実行する.
        /// </summary>
        /// <param name="argsList">実行するコマンドのリスト</param>
        /// <param name="currentSnapshot">コマンド実行時のカレントスナップショット</param>
        /// <returns>最後に出力されたスナップショット</returns>
        private Snapshot ExecCommand(IEnumerable<CommandLineArgs> argsList, Snapshot currentSnapshot)
        {
            var current = CurrentSnapshot;
            bool firstCommand = false;

            foreach (var cmd in argsList)
            {
                if (cmd.CommandName[0] == '#')
                {
                    if (firstCommand)
                    {
                        // 最初のコマンドにスナップショットが指定された場合、そこにカレントを移動する
                        current = current.FindSnapshot(cmd.CommandName.Length == 1 ? null : cmd.CommandName.Substring(1));
                        if (current == null)
                        { throw new SnapshotNotFoundException(cmd.CommandName.Substring(1)); }
                    }
                    else
                    {
                        // 2番目以降の場合、直前に出力されたスナップショットの名前を変更する
                        if (1 < cmd.CommandName.Length)
                        { current.Name = cmd.CommandName.Substring(1); }
                        else
                        { }
                    }
                }
                else
                {
                    if (ExecMetaCommand(cmd))
                    {
                        // MetaCommand の実行を試みる
                        // コマンド実行後、カレントスナップショットが変更されている可能性があるので再設定する
                        current = this.CurrentSnapshot;
                        continue;
                    }
                    else
                    {
                        // MetaCommand が見つからなかった場合は ManipulationCommand の実行を試みる
                        // コマンドの実行結果をカレントの下に加え、カレントを移動する
                        var newSnapshot = this._cmdController.ExecuteManipulationCommand(current, cmd.CommandName, cmd.Args.ToArray());
                        if (current != newSnapshot)
                        {
                            newSnapshot.Operation = cmd;
                            current = this.CurrentDocument.AddSnapshot(current, newSnapshot);

                            // スナップショットが追加された時点で保存コマンドを使用可能にする
                            this.FileSaveAsCommand.RaiseCanExecuteChanged();
                            this.FileSaveCommand.RaiseCanExecuteChanged();
                        }
                    }
                }
                firstCommand = false;
            }
            return current;
        }

        /// <summary>
        /// 指定したスナップショットをカレントに設定する
        /// </summary>
        /// <param name="newCurrent"></param>
        public void SetCurrentSnapshot(Snapshot newCurrent)
        {
            this.CurrentDocument.Current = newCurrent;
            OnPropertyChanged("CurrentSnapshot");

            if (this.Parent != null)
            { this.Parent.IE.NavigateToString(BrowserUtils.RenderText(newCurrent, this.TabSize)); }
        }

        /// <summary>
        /// アプリケーションの設定ファイルを読み込む
        /// </summary>
        private void LoadConfig()
        {
            // テキストファイルのエンコーディング
            // 指定された名前をエンコーディング名として解決出来なかった場合はデフォルト値として日本語自動判別にする
            try
            {
                var encodingName = Properties.Settings.Default.ENCODING;
                this.DefaultEncoding = (string.IsNullOrWhiteSpace(encodingName) ? null : encodingName.ToEncoding());
            }
            catch (Exception)
            { this.DefaultEncoding = null; }

            // タブ文字の幅
            {
                var tabSize = Properties.Settings.Default.TAB_SIZE;
                this.TabSize = (0 < tabSize ? tabSize : 4);
            }

        }

        /// <summary>
        /// コマンドのヘルプを表示する
        /// </summary>
        private void ShowHelp()
        {
            var selectedCommandLine = GetSelectedCommandLine(
                this.Parent.CommandLine.Text,
                this.Parent.CommandLine.SelectionStart
                ).Trim();
            HelpBrowserUtil.ShowHelp(selectedCommandLine, this._cmdController);
        }

        /// <summary>
        /// 指定した文字列の選択位置にあるコマンドラインを選択する
        /// </summary>
        /// <param name="commandLine">TextBoxに入力したコマンドライン文字列</param>
        /// <param name="selectedPosition">TextBoxの現在のカーソル位置</param>
        /// <returns>カーソル位置にあるコマンドライン文字列</returns>
        private string GetSelectedCommandLine(string commandLine, int selectedPosition)
        {
            string[] commands = commandLine.Split(new char[] { '|' }, StringSplitOptions.None);
            var startPosition = 0;
            foreach (var cmd in commands)
            {
                if (selectedPosition <= startPosition + cmd.Length)
                { return cmd; }
                startPosition += cmd.Length + 1;
            }
            return "";
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainWindowViewModel()
        {
            this.FileOpenCommand = new RelayCommand(x => true, (x) => { ExecFileOpen(); });
            this.FileSaveCommand = new RelayCommand(
                (x) => { return (this.CurrentDocument != null && this.CurrentDocument.IsModified); },
                (x) => { ExecFileSave(); });
            this.FileSaveAsCommand = new RelayCommand(
                (x) => { return (this.CurrentDocument != null && this.CurrentDocument.IsModified); },
                (x) => { ExecFileSaveAs(); });
            this.ExitCommand = new RelayCommand(x => true, (x) => { ExecFileExit(); });
            this.ExecEditCommand = new RelayCommand(x => true, (x) => { ExecEdit(); });
            this.CommandHelpCommand = new RelayCommand(x => true, (x) => { ShowHelp(); });

            // アプリケーションの設定を読み込む
            LoadConfig();

            // コマンドのコントローラを生成
            this._cmdController = new CommandController().Init();
        }
    }
}

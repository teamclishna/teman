﻿using System;
using System.Linq;
using System.Text;

namespace TeMan.utils
{
    /// <summary>
    /// 文字列操作に関するユーティリティクラス
    /// </summary>
    public static class StringUtils
    {
        /// <summary>半角文字の判定に使うShiftJISエンコーディング</summary>
        private static Encoding sjis = Encoding.GetEncoding("Shift_JIS");

        /// <summary>
        /// 指定した行の文字幅をカウントする.
        /// </summary>
        /// <param name="str">行文字列</param>
        /// <param name="useZenkaku">
        /// 全角文字を2文字として扱うなら true.
        /// 全角文字であっても1文字とする (.net Frameworkにおける文字数でカウントする) 場合は false.
        /// </param>
        /// <returns>行の文字幅</returns>
        public static int StringWidth(this String str, bool useZenkaku)
        { return str.ToCharArray().StringWidth(useZenkaku); }

        /// <summary>
        /// 指定した行の文字幅をカウントする.
        /// </summary>
        /// <param name="chars">行文字列の配列</param>
        /// <param name="useZenkaku">
        /// 全角文字を2文字として扱うなら true.
        /// 全角文字であっても1文字とする (.net Frameworkにおける文字数でカウントする) 場合は false.
        /// </param>
        /// <returns>行の文字幅</returns>
        public static int StringWidth(this char[] chars, bool useZenkaku)
        { return chars.Select(x => x.CharacterWidth(useZenkaku)).Sum(); }

        /// <summary>
        /// 指定した文字の文字幅 (半角を1文字とした表示桁数) を返す
        /// </summary>
        /// <param name="c">判別対象の文字</param>
        /// <param name="useZenkaku">
        /// 全角文字を2文字として扱うなら true.
        /// 全角文字であっても1文字とする (.net Frameworkにおける文字数でカウントする) 場合は false.
        /// </param>
        /// <returns>指定した文字の文字幅</returns>
        public static int CharacterWidth(this char c, bool useZenkaku)
        {
            if (!useZenkaku)
            {
                // 全角／半角の区別をしない場合は1文字
                return 1;
            }
            else if (c < 128)
            {
                // 128未満なら1文字(ASCII)
                return 1;
            }
            else
            {
                // SJISエンコーディングを試みてその文字数を返す
                // エンコーディングに失敗したら1文字として返す
                try
                { return sjis.GetByteCount(new char[] { c }); }
                catch
                { return 1; }
            }
        }
    }
}

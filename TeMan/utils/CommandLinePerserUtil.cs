﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeMan.utils
{
    /// <summary>
    /// コマンドラインのパース結果
    /// </summary>
    public class CommandLineArgs
    {
        /// <summary>
        /// 実行するコマンド名.
        /// 別のスナップショットを指定する場合、#記号で始まるスナップショット名をここに入れる.
        /// </summary>
        public string CommandName { get; set; }

        /// <summary>
        /// コマンドの引数.
        /// </summary>
        public IEnumerable<string> Args { get; set; }

        /// <summary>
        /// コマンドライン文字列.
        /// コマンドと引数をスペース区切りの文字列に連結して返す.
        /// </summary>
        public string CommandlineString { get { return this.ToCommandlineString(); } }
    }

    /// <summary>
    /// 編集コマンドをパースするためのユーティリティクラス
    /// </summary>
    public static class CommandLinePerserUtil
    {
        public static IEnumerable<CommandLineArgs> Parse(string cmdLine)
        {
            // 文字列 → パイプ単位に分割
            var piped = SplitCharAray(cmdLine);

            // パイプごとのコマンドライン → 引数単位にパース
            var commandLines = new List<IEnumerable<string>>();
            foreach (var ca in piped)
            { commandLines.Add(JoinToStrings(ca)); }

            // パースした配列 → コマンドライン情報
            var result = new List<CommandLineArgs>();
            foreach (var args in commandLines)
            { result.Add(ToCommandLineArgs(args)); }

            return result;
        }

        /// <summary>
        /// コマンドライン文字列を文字単位に分割し、パイプ記号で分割したリストに格納して返す
        /// </summary>
        /// <param name="cmdLine">コマンドライン文字列</param>
        /// <returns></returns>
        private static IEnumerable<IEnumerable<char>> SplitCharAray(string cmdLine)
        {
            var ca = cmdLine.ToCharArray();
            var result = new List<List<char>>();

            if (cmdLine.Length == 0)
            { return result; }

            List<char> current = new List<char>();
            bool inQuote = false;
            foreach(var c in ca)
            {
                if (c == '|' && !inQuote)
                {
                    // パイプ記号が現れたら文字列を格納してたリストを新しくする
                    result.Add(current);
                    current = new List<char>();
                }
                else
                {
                    // この時点ではダブルクォート文字列の一部として返す.
                    // ただし、文字列中のパイプ記号とコマンドラインの区切りを判別するために
                    // クォートのフラグはチェックする
                    if (c == '\"')
                    { inQuote = !inQuote; }

                    current.Add(c);
                }
            }

            result.Add(current);
            return result;
        }

        /// <summary>
        /// char の配列からコマンドラインとして使う文字列のリストを生成して返す.
        /// </summary>
        /// <param name="charArray"></param>
        /// <returns></returns>
        private static IEnumerable<string> JoinToStrings(IEnumerable<char> charArray)
        {
            List<string> result = new List<string>();
            List<char> current = null;
            bool inQuote = false;
            bool escaped = false;

            foreach (var c in charArray)
            {
                switch (c)
                {
                    case ' ':
                        if (inQuote)
                        {
                            // スペースはクォート内の場合だけ追加
                            current.Add(c);
                        }
                        else
                        {
                            // クォート内以外は引数の区切り
                            if (current != null)
                            {
                                result.Add(new string(current.ToArray()));
                                current = null;
                            }
                        }
                        break;

                    case '\"':
                        if (escaped)
                        {
                            // エスケープ中であればそのまま引数に追加
                            if (current == null)
                            { current = new List<char>(); }

                            current.Add(c);
                            escaped = false;
                        }
                        else
                        { inQuote = !inQuote; }

                        break;

                    case '\\':
                        if(inQuote)
                        {
                            if(escaped)
                            {
                                // エスケープ中であればそのまま引数に追加
                                if (current == null)
                                { current = new List<char>(); }

                                current.Add(c);
                            }
                            else
                            {
                                // 次の文字をエスケープする.
                                // ただし、エスケープ文字を有効にするのはクォートされている文字列内のみとする
                                escaped = true;
                            }
                        }
                        else
                        {
                            // クォートされている文字列内以外は単なる円記号として追加する
                            if (current == null)
                            { current = new List<char>(); }

                            current.Add(c);
                        }
                        break;

                    default:
                        if (current == null)
                        { current = new List<char>(); }

                        current.Add(c);
                        escaped = false;
                        break;
                }
            }

            if (current != null)
            { result.Add(new string(current.ToArray())); }

            return result;
        }

        /// <summary>
        /// コマンドライン引数をパースした文字列の配列を構造体に格納しなおす
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static CommandLineArgs ToCommandLineArgs(IEnumerable<string> args)
        {
            return new CommandLineArgs()
            {
                CommandName = args.ElementAt(0).ToLower(),
                Args = args.Skip(1),
            };
        }

        public static string ToCommandlineString(this CommandLineArgs cmd)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(cmd.CommandName).Append(" ");

            // コマンドライン引数を連結する
            // ダブルクォート、スペースが含まれる場合はダブルクォートで囲む
            // 引数中のダブルクォートは円記号を付ける
            foreach(var arg in cmd.Args)
            {
                var useQuote = (arg.Contains(' ') || arg.Contains('\"'));
                var escaped = arg.Replace("\"", "\\\"");

                if (useQuote) { sb.Append('\"'); }
                sb.Append(escaped);
                if (useQuote) { sb.Append('\"'); }
                sb.Append(' ');
            }

            return sb.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TeMan.model;

namespace TeMan.utils
{
    /// <summary>
    /// テキストファイルの読み込み、保存を行うためのユーティリティクラス
    /// </summary>
    public static class FileUtils
    {
        /// <summary>エンコーディング名 (Shift-JIS)</summary>
        public static readonly string ENCODING_NAME_SJIS = "Shift_jis";
        /// <summary>エンコーディング名 (EUC)</summary>
        public static readonly string ENCODING_NAME_EUC = "euc-jp";

        /// <summary>エンコーディング名 (utf-8 BOMなし)</summary>
        public static readonly string ENCODING_NAME_UTF8N = "utf-8n";

        /// <summary>エンコーディング名 (utf-8 BOMなし)</summary>
        public static readonly string ENCODING_NAME_UTF8N2 = "utf_8n";

        /// <summary>
        /// 指定したエンコーディング名に対応する Encoding オブジェクトを返す.
        /// </summary>
        /// <param name="encodingName"></param>
        /// <returns></returns>
        public static Encoding ToEncoding(this string encodingName)
        {
            // utf-8Nの場合は明示的にBOM無しを生成する
            return (
                encodingName.Equals(ENCODING_NAME_UTF8N) ? new UTF8Encoding(false) :
                encodingName.Equals(ENCODING_NAME_UTF8N2) ? new UTF8Encoding(false) :
                Encoding.GetEncoding(encodingName));
        }

        /// <summary>
        /// テキストファイル自動判別した日本語エンコーディングでロードし、Documetオブジェクトを生成する.
        /// </summary>
        /// <param name="fi">読み込み対象のファイル</param>
        /// <returns>読み込んだファイルのDocumentオブジェクト</returns>
        public static Document Load(this FileInfo fi)
        { return Load(fi, DetectJapanese(fi)); }

        /// <summary>
        /// テキストファイルをロードし、Documentオブジェクトを生成する
        /// </summary>
        /// <param name="fi">読み込み対象のファイル</param>
        /// <param name="enc">ファイルのエンコーディング</param>
        /// <returns>読み込んだファイルのDocumentオブジェクト</returns>
        public static Document Load(this FileInfo fi, Encoding enc)
        {
            var t = new StreamReader(fi.FullName, enc);

            // ドキュメントを生成し、最初の状態のスナップショットを生成する
            var doc = new Document();
            var rootSnapshot = new Snapshot() { IsSelected = true, };
            var lineNumber = 1L;
            while (!t.EndOfStream)
            {
                var l = t.ReadLine();
                var newLine = new model.Line(lineNumber, l);

                rootSnapshot.Lines.Add(newLine);
                lineNumber++;
            }

            doc.OpenFile = fi;
            doc.FileEncoding = enc;
            doc.Snapshots.Add(rootSnapshot);
            doc.Current = rootSnapshot;

            return doc;
        }

        /// <summary>
        /// 指定したドキュメントのカレントスナップショットを上書き保存する
        /// </summary>
        /// <param name="doc">保存対象のドキュメント</param>
        public static void Save(this Document doc)
        { Save(doc, doc.Current); }

        /// <summary>
        /// 指定したドキュメントの指定したスナップショットを上書き保存する
        /// </summary>
        /// <param name="doc">保存対象のドキュメント</param>
        /// <param name="snapshot">doc 中の保存対象のスナップショット</param>
        public static void Save(
            this Document doc,
            Snapshot snapshot)
        { Save(doc, doc.Current, doc.OpenFile, doc.FileEncoding); }

        /// <summary>
        /// スナップショットをファイルに保存する
        /// </summary>
        /// <param name="doc">保存対象のドキュメント</param>
        /// <param name="snapshot">doc 中の保存対象のスナップショット</param>
        /// <param name="fi">保存先のファイルパス</param>
        /// <param name="enc">保存時のテキストエンコーディング</param>
        public static void Save(
            this Document doc,
            Snapshot snapshot,
            FileInfo fi,
            Encoding enc)
        {
            using (var w = new StreamWriter(fi.FullName, false, enc))
            {
                foreach (var l in snapshot.Lines)
                { w.WriteLine(l.Text); }

                w.Close();
            }
        }

        /// <summary>
        /// 指定したファイルを開き、文字コードを自動判別する.
        /// </summary>
        /// <param name="fi">チェック対象のファイル</param>
        /// <returns>判別結果. 判別できなかった場合は utr-8 として返す.</returns>
        public static Encoding DetectJapanese(this FileInfo fi)
        {
            // ファイルの先頭から最大1024バイトを読み込んで文字コードを判別する
            using (var stream = new FileStream(fi.FullName, FileMode.Open))
            {
                byte[] b = new byte[1024];

                var readSize = stream.Read(b, 0, b.Length);
                var index = 0;
                while(index < readSize)
                {
                    var lead = (int)b[index];

                    if (0x80 <= lead && lead <= 0x9f)
                    {
                        // [0x80, 0x9f] が現れたら SJIS
                        return Encoding.GetEncoding(ENCODING_NAME_SJIS);
                    }
                    else if (0xa1 <= lead && lead <= 0xbf)
                    {
                        index++;
                        var trail = (int)b[index];

                        // [0xa1, 0xbf], [0xa1, 0xfe] の場合は EUC
                        if (0xa1 <= trail && trail <= 0xfe)
                        { return Encoding.GetEncoding(ENCODING_NAME_EUC); }
                    }
                    else if (0xc0 <= lead && lead <= 0xdf)
                    {
                        index++;
                        var trail = (int)b[index];

                        // [0xc0, 0xdf], [0x80, 0x9f] の場合は utf-8
                        // [0xc0, 0xdf], [0xa0, 0xff] の場合は EUC
                        if (0x80 <= trail && trail <= 0x9f)
                        { return new UTF8Encoding(false); }

                        if (0xa0 <= trail && trail <= 0xff)
                        { return Encoding.GetEncoding(ENCODING_NAME_EUC); }
                    }
                    else if (0xe0 <= lead && lead <= 0xef)
                    {
                        index++;
                        var trail = (int)b[index];

                        // [0xe0, 0xef], [0x80, 0xbf] の場合は utf-8
                        // [0xe0, 0xef], [0x40, 0xff] の場合は SJIS
                        if (0x80 <= trail && trail <= 0xbf)
                        { return new UTF8Encoding(false); }

                        if (0x40 <= trail && trail <= 0xff)
                        { return Encoding.GetEncoding(ENCODING_NAME_SJIS); }
                    }
                    else if (0xf0 <= lead && lead <= 0xfe)
                    {
                        index++;
                        var trail = (int)b[index];

                        // [0xf0, 0xfe], [0xc0, 0xfe] の場合は EUC
                        if (0xc0 <= trail && trail <= 0xfe)
                        { return Encoding.GetEncoding(ENCODING_NAME_EUC); }
                    }
                    index++;
                }

                // 確定しなかった場合は utf-8N とする
                return new UTF8Encoding(false);
            }
        }
    }
}

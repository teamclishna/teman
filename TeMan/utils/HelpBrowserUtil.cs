﻿using System.Diagnostics;
using System.Windows.Forms;

using TeMan.Properties;
using TeMan.commands;

namespace TeMan.utils
{
    /// <summary>
    /// ヘルプ表示のためのクラス
    /// </summary>
    public static class HelpBrowserUtil
    {
        /// <summary>
        /// 指定したコマンドに対するヘルプを開く
        /// </summary>
        /// <param name="commandLine">ヘルプの対象とするコマンドライン. キーワードを指定せずトップページを開く場合は空文字列</param>
        /// <param name="ctrl">コマンドコントローラ</param>
        public static void ShowHelp(string commandLine, CommandController ctrl)
        {
            var helpTopic = commandLine.Trim().Split(new char[] { ' ' })[0];
            var commandType = ctrl.GetCommandType(helpTopic);

            if (commandType == CommandType.NOT_IMPL)
            { Help.ShowHelp(null, Resources.HELP_FILE_NAME, HelpNavigator.Index); }
            else
            { Help.ShowHelp(null, Resources.HELP_FILE_NAME, HelpNavigator.KeywordIndex, helpTopic); }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TeMan.model;

namespace TeMan.utils
{
    /// <summary>
    /// ドキュメント、スナップショットに関するユーティリティクラス
    /// </summary>
    public static class DocumentUtils
    {
        /// <summary>
        /// 指定したスナップショットのの親子を検索し、指定した名前のスナップショットを検索する.
        /// </summary>
        /// <param name="snapshots">検索元のスナップショット</param>
        /// <param name="snapshotName">検索対象のスナップショット名. 最上位 (root) を返す場合は null を指定する.</param>
        /// <returns>検索対象のスナップショット. 該当のスナップショットが見つからなかった場合は null</returns>
        public static Snapshot FindSnapshot(this Snapshot snapshots, string snapshotName)
        {
            // 最初にルートを取得し、そこから子孫を再帰的に検索する
            var root = GetRoot(snapshots);
            if(snapshotName == null)
            { return root; }

            return FindSnapshotToChildren(root, snapshotName);
        }

        /// <summary>
        /// 指定したスナップショットの子孫をたどり、指定した名前のスナップショットを検索する.
        /// </summary>
        /// <param name="snapshots">検索元のスナップショット</param>
        /// <param name="snapshotName">検索対象のスナップショット名.</param>
        /// <returns></returns>
        public static Snapshot FindSnapshotToChildren(Snapshot current, string snapshotName)
        {
            if (snapshotName.Equals(current.Name))
            { return current; }

            foreach (var child in current.Children)
            {
                if (snapshotName.Equals(child.Name))
                { return child; }
            }

            return null;
        }

        /// <summary>
        /// 指定したスナップショットの親を再帰的に辿り、最上位のスナップショットを返す.
        /// </summary>
        /// <param name="current">検索元のスナップショット.</param>
        /// <returns>スナップショットの最上位</returns>
        private static Snapshot GetRoot(Snapshot current)
        { return (current.Parent == null ? current : GetRoot(current.Parent)); }

        /// <summary>
        /// 指定した行番号のインデックスを返す.
        /// </summary>
        /// <param name="source">検索対象のスナップショット</param>
        /// <param name="lineNumber">
        /// 検索対象の行番号.
        /// ここで指定する行番号はスナップショット中の要素のインデックスではなく、各行の論理行番号である
        /// </param>
        /// <returns>該当する行情報の0から始まるインデックス. 該当の行が見つからなかった場合は負の値.</returns>
        public static int FindLine(this Snapshot source, long lineNumber)
        {
            int currentIndex = 0;
            foreach(var l in source.Lines)
            {
                if(l.LineNumber == lineNumber)
                { return currentIndex; }
                currentIndex++;
            }
            return -1;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

using TeMan.model;

namespace TeMan.utils
{
    /// <summary>
    /// テキスト表示に使用するWebBrowserコントロールに関するユーティリティクラス
    /// </summary>
    public static class BrowserUtils
    {
        /// <summary>テキストをブラウザに表示するためのHTMLのテンプレート</summary>
        private static readonly string HTML_TEMPLATE = @"<!DOCTYPE html><html>
<head>
<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />
<style>
	div.line {{ line-height:1.1em; }}
	div.linenumber {{ padding:0 1ex; }}
	code {{ font-family:Consolas; white-space:pre; tab-size:{2}; }}
    td.linenumber {{ text-align:right; }}
    span.highlight {{ background-color:yellow; }}
</style>
</head>
<body>
<table><tr><td class=""linenumber"">{0}</td><td>{1}</td></tr></table>
</body>
</html>
";

        /// <summary>行番号を表示するためのHTML</summary>
        private static readonly string HTML_LINE_HEADER = "<div class=\"line linenumber\"><code>{0}</code></div>";

        /// <summary>1行分のテキストを表示するためのHTML</summary>
        private static readonly string HTML_LINE_CONTENT = "<div class=\"line\"><code>{0}</code></div>";

        /// <summary>
        /// 指定したスナップショットをブラウザで表示するためのHTMLを生成する
        /// </summary>
        /// <param name="target">生成対象のテキスト</param>
        /// <param name="tabSize">タブ文字を表示するときの幅</param>
        /// <returns>生成したHTML</returns>
        public static string RenderText(Snapshot target, int tabSize)
        {
            var linesColumn = new StringBuilder();
            var contentColumn = new StringBuilder();

            foreach (var line in target.Lines)
            {
                // HTMLタグに相当する部分は実態参照に変換
                // 空行は単一の開業に変換する
                linesColumn.Append(string.Format(HTML_LINE_HEADER, line.LineNumber));
                contentColumn.Append(string.Format(HTML_LINE_CONTENT, RenderHighlight(line)));
            }

            return string.Format(HTML_TEMPLATE, linesColumn, contentColumn, tabSize);
        }

        /// <summary>
        /// 1行分の文字列を示すHTML文字列を生成する.
        /// ハイライト表示部分の背景に色を付ける.
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        private static string RenderHighlight(Line l)
        {
            if (0 < l.Text.Length)
            {
                var sb = new StringBuilder();

                foreach (var w in l.Highlights)
                {
                    if (w.IsHighlight == HighlightType.Highlight) { sb.Append("<span class=\"highlight\">"); }

                    var lineString = w.Text
                        .Replace("&", "&amp;")
                        .Replace("<", "&lt;")
                        .Replace(">", "&gt;");
                    sb.Append(lineString);

                    if (w.IsHighlight == HighlightType.Highlight) { sb.Append("</span>"); }
                }

                return sb.ToString();
            }
            else
            { return "<br />"; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.gui;
using clUtils.mvvm;

using TeMan.utils;

namespace TeMan.model
{
    /// <summary>
    /// テキスト操作中の1つのスナップショット
    /// </summary>
    public class Snapshot : NotifiableViewModel
    {
        #region Fields

        private string _name;
        private bool _isSelected;

        #endregion

        /// <summary>
        /// このスナップショットが現在選択状態かどうか?
        /// TreeView上でカレントアイテムを設定するために使うs
        /// </summary>
        public bool IsSelected
        {
            get { return this._isSelected; }
            set
            {
                this._isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        /// <summary>
        /// このスナップショット名
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set
            {
                this._name = value;
                OnPropertyChanged("Name");
                OnPropertyChanged("DisplayName");
            }
        }

        /// <summary>
        /// このスナップショットの表示名.
        /// ツリービュー上に表示する名称.
        /// </summary>
        public string DisplayName
        {
            get { return (this.Name == null ? "(root)" : "#" + this.Name); }
        }

        /// <summary>
        /// このスナップショットの前の操作に相当するスナップショット.
        /// 最初の操作 (ファイルを開いた直後とか) の場合はnull.
        /// </summary>
        public Snapshot Parent { get; set; }

        /// <summary>
        /// このスナップショットから別の操作を行った結果のスナップショット群.
        /// </summary>
        public ObservableCollection<Snapshot> Children { get; set; }

        /// <summary>
        /// このスナップショットを取得するために実行したコマンド.
        /// 最初の操作の場合はnull.
        /// </summary>
        public CommandLineArgs Operation { get; set; }

        /// <summary>
        /// このスナップショットの結果を示すテキスト行の集合.
        /// </summary>
        public ObservableCollection<Line> Lines { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Snapshot()
        {
            this.Children = new ObservableCollection<Snapshot>();
            this.Lines = new ObservableCollection<Line>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeMan.model
{
    public enum HighlightType
    {
        /// <summary>強調表示しない</summary>
        Normal,
        /// <summary>強調表示する</summary>
        Highlight,
    }

    /// <summary>
    /// 行文字列の中のハイライト表示属性.
    /// ハイライトしない箇所も IsHighlight プロパティを「強調表示しない」属性でこのクラスに格納する
    /// </summary>
    public class Highlight
    {
        /// <summary>ハイライト対象の文字列</summary>
        public string Text { get; private set; }
        
        /// <summary>ハイライト表示するかどうか</summary>
        public HighlightType IsHighlight { get; private set; }

        public Highlight(string t, HighlightType ht)
        {
            this.Text = t;
            this.IsHighlight = ht;
        }
    }

    /// <summary>
    /// テキストファイル中の1行を示すオブジェクト.
    /// 原則としてこのオブジェクトは不変である (何か操作されたら新たな Snapshot オブジェクトとしてファイル丸ごと複製する) ので、
    /// このオブジェクトはプロパティ変更の通知を行わない
    /// </summary>
    public class Line
    {
        /// <summary>
        /// 行番号
        /// </summary>
        public long LineNumber { get; private set; }

        /// <summary>
        /// テキストの行文字列
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// ハイライト表示の属性
        /// </summary>
        public List<Highlight> Highlights { get; private set; }

        /// <summary>
        /// コンストラクタ.
        /// 行文字列だけを指定し、行番号は0、ハイライトしない行情報を生成する.
        /// </summary>
        /// <param name="text">テキストの行番号</param>
        public Line(string text) : this(0, text)
        { }

        /// <summary>
        /// コンストラクタ.
        /// 行番号と行文字列を指定し、ハイライトしない行情報を生成する.
        /// </summary>
        /// <param name="lineNumber">テキストの行番号</param>
        /// <param name="text">テキスト文字列</param>
        public Line(long lineNumber, string text) : this(
            lineNumber,
            text,
            new Highlight[] { new Highlight(text, HighlightType.Normal) })
        { }

        /// <summary>
        /// コンストラクタ.
        /// 行番号、行文字列、行内のハイライト情報を持つ行情報を生成する.
        /// </summary>
        /// <param name="lineNumber">テキストの行番号</param>
        /// <param name="text">テキスト文字列</param>
        /// <param name="highlights">この行のハイライト情報</param>
        public Line(long lineNumber, string text, IEnumerable<Highlight> highlights)
        {
            this.LineNumber = lineNumber;
            this.Text = text;
            this.Highlights = new List<Highlight>(highlights);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.gui;
using clUtils.mvvm;

namespace TeMan.model
{
    /// <summary>
    /// アプリケーションで開いたファイル
    /// </summary>
    public class Document : NotifiableViewModel
    {
        #region Fields

        private long _seq = 0;

        #endregion

        /// <summary>
        /// このドキュメントの入力元であるファイル
        /// </summary>
        public FileInfo OpenFile { get; set; }

        /// <summary>
        /// ファイルを開いたときのエンコーディング
        /// </summary>
        public Encoding FileEncoding { get; set; }

        /// <summary>
        /// このファイルの操作履歴を格納するスナップショット.
        /// </summary>
        public ObservableCollection<Snapshot> Snapshots { get; set; }

        /// <summary>
        /// Snapshots の階層下で、リストビューで現在表示中のスナップショット
        /// </summary>
        public Snapshot Current { get; set; }

        /// <summary>
        /// このドキュメント内で生成したスナップショットの通し番号.
        /// スナップショット名を指定せずに生成した場合、この番号を元に作る.
        /// プロパティにアクセスするたびに値を加算するので、ドキュメント内のスナップショットが常に連続した番号になるとは限らない.
        /// </summary>
        public long Sequence
        { get { return ++this._seq; } }

        /// <summary>
        /// このドキュメントが変更されていれば true.
        /// 「変更されている」とは、root以外のスナップショットが存在することを指す.
        /// </summary>
        public bool IsModified
        { get { return (1 < this.Snapshots.Count || 0 < this.Snapshots[0].Children.Count); } }

        /// <summary>
        /// 指定したスナップショットの編集結果を追加する
        /// </summary>
        /// <param name="current">スナップショットの追加元になるオブジェクト</param>
        /// <param name="newSnapshot">追加するスナップショット</param>
        /// <returns>
        /// newSnapshot のインスタンス.
        /// このインスタンスには親として current が設定されている.
        /// また、スナップショット名が null の場合、仮の名称として連番が割り当てられる.
        /// </returns>
        public Snapshot AddSnapshot(Snapshot current, Snapshot newSnapshot)
        {
            current.Children.Add(newSnapshot);
            newSnapshot.Parent = current;
            if (newSnapshot.Name == null)
            { newSnapshot.Name = this.Sequence.ToString(); }

            this.Current = newSnapshot;
            OnPropertyChanged("Snapshots");
            OnPropertyChanged("Current");

            return newSnapshot;
        }


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Document()
        {
            this.Snapshots = new ObservableCollection<Snapshot>();
        }
    }
}

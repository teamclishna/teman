﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;

using clUtils.util;

using TeMan.model;
using TeMan.utils;
using TeMan.vm;

namespace TeMan
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// スナップショットを表示するためのツリービュー
        /// </summary>
        public TreeView Snapshots { get { return this.tv; } }

        /// <summary>
        /// 現在のスナップショットを表示するブラウザ
        /// </summary>
        public WebBrowser IE { get { return this.ie; } }

        /// <summary>
        /// コマンドラインを入力するテキストボックス
        /// </summary>
        public TextBox CommandLine { get { return this.commandLine; } }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // 初期状態で空のViewModelを生成して設定しておく
            var vm = new MainWindowViewModel() { Parent = this, NotifyMessage = Properties.Resources.MSG_READY };
            this.DataContext = vm;

            // コマンドライン引数で起動対象のファイルが指定されていれば、この時点でファイルを読み込んでViewModelに格納しておく
            var parsedArgs = CommandLineUtils.Parse(
                Environment.GetCommandLineArgs(),
                new CommandLineOptionDefinition[] {
                    new CommandLineOptionDefinition() { LongSwitch = "encoding", RequireParam = true, },
                });

            var encodingName = (
                parsedArgs.Options.ContainsKey("encoding") ? parsedArgs.Options["encoding"].ElementAt(0) :
                null);
            foreach (var openFile in parsedArgs.Args.Skip(1))
            { vm.Open(new string[] { openFile }, encodingName); }
        }

        /// <summary>
        /// ウィンドウにファイルがドロップされたときのイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var droppedData = e.Data.GetData(DataFormats.FileDrop) as string[];
                if(droppedData != null)
                {
                    var vm = this.DataContext as MainWindowViewModel;
                    vm.Open(droppedData, null);
                }
            }
        }

        /// <summary>
        /// ウィンドウに対して何らかのオブジェクトがドラッグされている時のイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var draggingData = e.Data.GetData(DataFormats.FileDrop);
                e.Effects = DragDropEffects.Copy;
            }
            else
            { e.Effects = DragDropEffects.None; }
        }

        /// <summary>
        /// ブラウザのドキュメントを表示するときのイベント.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ie_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            if (e.Uri == null)
            {
                // URI が null の場合はスナップショット表示のために HTML 文字列を渡しているので何もせず終了
                e.Cancel = false;
            }
            else
            {
                // ファイルをドロップされた場合はローカルパスに変換してそのファイルを開く
                var scheme = e.Uri.Scheme.ToLower();
                if (scheme.Equals("file"))
                {
                    var vm = this.DataContext as MainWindowViewModel;
                    vm.Open(new string[] { e.Uri.LocalPath + Uri.UnescapeDataString(e.Uri.Fragment) }, null);
                }
                e.Cancel = true;
            }
        }

        /// <summary>
        /// ツリービューのアイテムが選択されたときのイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Tv_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var selectedItem = this.tv.SelectedItem as Snapshot;
            if (selectedItem == null)
            {
                // 選択されているアイテムがnullの場合は何もしない
                // フォーカスが解除されたときとか？
                return;
            }

            // 選択したスナップショットをブラウザに表示する
            var vm = this.DataContext as MainWindowViewModel;
            vm.SetCurrentSnapshot(selectedItem);
        }

        private void CommandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                var vm = this.DataContext as MainWindowViewModel;
                vm.ExecEditCommand.Execute(this.commandLine);
            }
        }

        /// <summary>
        /// About ダイアログを表示する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuHelp_About_Click(object sender, RoutedEventArgs e)
        {
            new AboutWindow().ShowDialog();
        }

        /// <summary>
        /// スナップショットのツリービュー上で右クリックしたときのイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tv_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            // スナップショットの生成元になったコマンドラインを取得して
            // コマンドライン欄に表示する.
            var selectedItem = ((TreeView)sender).SelectedItem as Snapshot;
            if(selectedItem.Operation != null)
            {
                var vm = this.DataContext as MainWindowViewModel;
                vm.CommandLine = selectedItem.Operation.ToCommandlineString();
            }
        }
    }
}

﻿using System.Reflection;
using System.Windows;

namespace TeMan
{
    /// <summary>
    /// AboutWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class AboutWindow : Window
    {
        public AboutWindow()
        {
            InitializeComponent();

            // アプリケーション名とバージョン情報をダイアログに設定する
            var asmName = Assembly.GetExecutingAssembly().GetName();
            var versionInfo = asmName.Version;
            this.txtAppName.Text = asmName.Name;
            this.txtVersion.Text = string.Format(
                    "{0}.{1}.{2}.{3}",
                    versionInfo.Major,
                    versionInfo.Minor,
                    versionInfo.Build,
                    versionInfo.Revision);
        }

        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeMan.commands
{
    /// <summary>
    /// 指定したコマンドが存在しなかった場合にスローされる例外
    /// </summary>
    public class CommandNotFoundException : Exception
    {
        public string CommandName { get; private set; }

        public CommandNotFoundException(String cmdName) : base()
        { this.CommandName = cmdName; }
    }
}

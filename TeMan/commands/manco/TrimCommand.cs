﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using clUtils.util;
using TeMan.model;

namespace TeMan.commands
{
    /// <summary>
    /// 行内の空白の削除を行うコマンド
    /// </summary>
    public class TrimCommand : IManipulationCommand
    {
        public string CommandName => "trim";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[] {
            new CommandLineOptionDefinition() { LongSwitch = "removeemptyline", RequireParam = false, AllowMultiple = false, },
            new CommandLineOptionDefinition() { LongSwitch = "trimstart", RequireParam = false, AllowMultiple = false, },
            new CommandLineOptionDefinition() { LongSwitch = "trimend", RequireParam = false, AllowMultiple = false, },
        };

        /// <summary>
        /// 空白除去のモードを指定する列挙子
        /// </summary>
        private enum TrimMode
        {
            /// <summary>行の先頭の空白のみ削除</summary>
            START = 1,
            /// <summary>行の末尾の空白のみ削除</summary>
            END = 2,
            /// <summary>行の前後とも削除</summary>
            BOTH = 3,
        }

        public Snapshot Execute(Snapshot source, CommandLineOptions options, string[] args)
        {
            // 空行を削除する?
            var removeEmptyLine = options.Options.ContainsKey("removeemptyline");
            // 空白削除モード
            // スイッチに先頭、末尾ともついていない場合は両方を指定した場合と同じ動作にする
            var mode = (
                options.Options.ContainsKey("trimstart") ?
                    (options.Options.ContainsKey("trimend") ? TrimMode.BOTH : TrimMode.START) :
                    (options.Options.ContainsKey("trimend") ? TrimMode.END : TrimMode.BOTH)
                );

            var result = new Snapshot()
            {
                Name = null,
                Parent = source
            };

            foreach (var line in source.Lines)
            {
                var trimmed = TrimLine(line.Text, mode);

                // 空白行を削除？
                if(removeEmptyLine && trimmed.Length == 0)
                { continue; }

                result.Lines.Add(new Line(line.LineNumber, trimmed));
            }

            return result;
        }

        /// <summary>
        /// 行の空白を取り除く
        /// </summary>
        /// <param name="str"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        private string TrimLine(string str, TrimMode mode)
        {
            switch (mode)
            {
                case TrimMode.BOTH:
                    return str.Trim();
                case TrimMode.START:
                    return str.TrimStart(null);
                case TrimMode.END:
                    return str.TrimEnd(null);
                default:
                    return str.Trim();
            }
        }
    }
}

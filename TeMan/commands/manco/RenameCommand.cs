﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using clUtils.util;
using TeMan.model;

namespace TeMan.commands
{
    /// <summary>
    /// 現在のスナップショット名を変更するコマンド
    /// </summary>
    public class RenameCommand : IManipulationCommand
    {
        public string CommandName => "rename";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[] { };

        public Snapshot Execute(Snapshot source, CommandLineOptions options, string[] args)
        {
            // スナップショット名が null (=最上位) はリネーム不可
            if (source.Name == null)
            { throw new Exception(Properties.Resources.CMD_RENAME_ROOT_NODE); }
            else if (options.Args.Count() < 1)
            { throw new Exception(Properties.Resources.CMD_RENAME_NO_PARAM); }
            else
            { source.Name = options.Args.ElementAt(0); }
            return source;
        }
    }
}

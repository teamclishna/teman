﻿using System;
using System.Linq;
using System.Text;

using clUtils.util;

using TeMan.model;
using TeMan.utils;

namespace TeMan.commands.manco
{
    /// <summary>
    /// 指定した長さになるまで複数行を連結するコマンド
    /// </summary>
    public class ConcatCommand : IManipulationCommand
    {
        public string CommandName => "concat";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[] {
            new CommandLineOptionDefinition() { LongSwitch = "zenkaku", RequireParam = false, },
        };

        public Snapshot Execute(Snapshot source, CommandLineOptions options, string[] args)
        {
            // 全角文字を2文字として扱う?
            var useZenkaku = options.Options.ContainsKey("zenkaku");

            // 行の文字数
            if (options.Args.Count() < 1)
            { throw new ArgumentException(Properties.Resources.CMD_WRAP_NO_PARAM); }
            int wrapCount = 0;
            if (!int.TryParse(options.Args.ElementAt(0), out wrapCount) || wrapCount <= 0)
            { throw new ArgumentException(Properties.Resources.CMD_WRAP_NO_PARAM); }

            var result = new Snapshot()
            {
                Name = null,
                Parent = source
            };

            var sb = new StringBuilder();
            var currentLineNumber = 1L;
            var currentLineLength = 0;
            foreach (var line in source.Lines)
            {
                var lineLength = line.Text.StringWidth(useZenkaku);

                // 行の文字数が指定の文字数を超える場合はここまでの文字列を行とする
                if (wrapCount < currentLineLength + lineLength)
                {
                    result.Lines.Add(new Line(currentLineNumber++, sb.ToString()));
                    sb.Clear();
                }

                sb.Append(line.Text);
                currentLineLength += lineLength;
            }

            // 残っている行を追加
            result.Lines.Add(new Line(currentLineNumber++, sb.ToString()));

            return result;
        }
    }
}

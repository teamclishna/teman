﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using clUtils.util;
using TeMan.model;

namespace TeMan.commands
{
    /// <summary>
    /// ドキュメント内の重複要素を検索するコマンド
    /// </summary>
    public class UniqCommand : IManipulationCommand
    {
        public string CommandName => "uniq";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[]
        {
            new CommandLineOptionDefinition() { LongSwitch = "count", RequireParam = false, AllowMultiple = false, },
            new CommandLineOptionDefinition() { LongSwitch = "repeated", RequireParam = false, AllowMultiple = false, },
            new CommandLineOptionDefinition() { LongSwitch = "allrepeated", RequireParam = false, AllowMultiple = false, },
            new CommandLineOptionDefinition() { LongSwitch = "unique", RequireParam = false, AllowMultiple = false, },
            new CommandLineOptionDefinition() { LongSwitch = "field", RequireParam = true, AllowMultiple = false, },
            new CommandLineOptionDefinition() { LongSwitch = "delimiter", RequireParam = true, AllowMultiple = false, },
            new CommandLineOptionDefinition() { LongSwitch = "character", RequireParam = true, AllowMultiple = false, },
        };

        public Snapshot Execute(Snapshot source, CommandLineOptions options, string[] args)
        {
            // 出力時に重複した行数を追記する
            var outputCount = options.Options.ContainsKey("count");
            // 重複行を出力する
            var outputRepeaded = options.Options.ContainsKey("repeated");
            // 全ての重複行を出力する
            var outputRepeatedAll = options.Options.ContainsKey("allrepeated");
            // 重複しなかった行を出力する
            var outputUniqueOnly = options.Options.ContainsKey("unique");

            // 重複判定を指定したフィールドで行う 
            // フィールドの区切り文字は delimiter スイッチのパラメータを使用し、 delimiter が指定されていない場合は空白を区切り文字とする
            var fieldCount = (options.Options.ContainsKey("field") ? int.Parse(options.Options["field"].ElementAt(0)) : -1);
            var delimiter = (options.Options.ContainsKey("delimiter") ? options.Options["delimiter"].ElementAt(0).ToCharArray() : new char[] { ' ' });
            // 行の先頭から指定した文字数までを重複判定に使用する
            var charaCount = (options.Options.ContainsKey("character") ? int.Parse(options.Options["character"].ElementAt(0)) : 0);

            // 行出力のフラグ3種が複数指定されている場合はエラー
            // まったく指定されていない場合は重複行を取り除く (repeated + unique)
            if (1 <
                (outputRepeaded ? 1 : 0) +
                (outputRepeatedAll ? 1 : 0) +
                (outputUniqueOnly ? 1 : 0))
            { throw new ArgumentException(Properties.Resources.CMD_UNIQ_INVALID_ARGS); }
            else if (0 ==
                (outputRepeaded ? 1 : 0) +
                (outputRepeatedAll ? 1 : 0) +
                (outputUniqueOnly ? 1 : 0))
            {
                outputRepeaded = true;
                outputUniqueOnly = true;
            }

            var uniqCounts = CountUnique(source, fieldCount, delimiter, charaCount);
            var outputLines = Output(source, uniqCounts, outputCount, outputRepeaded, outputRepeatedAll, outputUniqueOnly, fieldCount, delimiter, charaCount);
            var result = new Snapshot()
            {
                Name = null,
                Parent = source
            };
            foreach(var line in outputLines)
            { result.Lines.Add(line); }

            return result;
        }

        /// <summary>
        /// 行ごとの出現回数のカウントを行う
        /// </summary>
        /// <param name="source">カウント対象のスナップショット</param>
        /// <param name="fieldCount">指定したフィールドを判定対象にする場合、0から始まるインデックス. フィールドで判定しない場合は負の値</param>
        /// <param name="delimiter">フィールドを判定対象にする場合、フィールド区切りのために使用する文字の集合</param>
        /// <param name="charaCount">行の先頭から指定した文字数までを判定対象にする場合、その文字数. 文字数で判定しない場合は0</param>
        /// <returns></returns>
        private Dictionary<string, long> CountUnique(Snapshot source, int fieldCount, char[] delimiter, int charaCount)
        {
            var result = new Dictionary<string, long>();

            foreach (var line in source.Lines)
            {
                string key = CreateKey(line.Text, fieldCount, delimiter, charaCount);
                if (result.ContainsKey(key))
                { result[key]++; }
                else
                { result.Add(key, 1); }
            }

            return result;
        }

        /// <summary>
        /// 行の出力を行う
        /// </summary>
        /// <param name="source">カウント対象のスナップショット</param>
        /// <param name="counts">キー毎の出現回数を格納した辞書</param>
        /// <param name="outputCount">出力時に重複件数を追記する場合はtrue</param>
        /// <param name="outputRepeated">重複行の最初の1回のみを出力する場合はtrue</param>
        /// <param name="outputRepeatedAll">全ての重複行を出力する場合はtrue</param>
        /// <param name="outputUniqueOnly">重複しない行のみを出力する場合はtrue</param>
        /// <param name="fieldCount">指定したフィールドを判定対象にする場合、0から始まるインデックス. フィールドで判定しない場合は負の値</param>
        /// <param name="delimiter">フィールドを判定対象にする場合、フィールド区切りのために使用する文字の集合</param>
        /// <param name="charaCount">行の先頭から指定した文字数までを判定対象にする場合、その文字数. 文字数で判定しない場合は0</param>
        /// <returns></returns>
        private List<Line> Output(
            Snapshot source,
            Dictionary<string, long> counts,
            bool outputCount,
            bool outputRepeated,
            bool outputRepeatedAll,
            bool outputUniqueOnly,
            int fieldCount,
            char[] delimiter,
            int charaCount)
        {
            var outputList = new List<Line>();
            var outputRepeatFlg = new HashSet<string>();

            string formatString = null;
            if(outputCount)
            {
                // 出現件数最大値のカウントを取得
                var maxLength = counts.Values.Max().ToString().Length;
                formatString = "{0," + maxLength.ToString() + "}";
            }

            foreach (var line in source.Lines)
            {
                var key = CreateKey(line.Text, fieldCount, delimiter, charaCount);
                var count = counts[key];

                if (1 == count)
                {
                    // 重複しない行
                    if (outputUniqueOnly)
                    { outputList.Add(CreateLine(line.LineNumber, line.Text, outputCount, count, formatString)); }
                }
                else
                {
                    // 重複 (2回以上出現した) 行
                    if (outputRepeatedAll)
                    { outputList.Add(CreateLine(line.LineNumber, line.Text, outputCount, count, formatString)); }
                    else if (outputRepeated)
                    {
                        if (!outputRepeatFlg.Contains(key))
                        {
                            outputList.Add(CreateLine(line.LineNumber, line.Text, outputCount, count, formatString));
                            outputRepeatFlg.Add(key);
                        }
                    }
                }
            }

            return outputList;
        }

        /// <summary>
        /// 行情報を生成する
        /// </summary>
        /// <param name="lineNumber">行番号</param>
        /// <param name="text">行文字列</param>
        /// <param name="outputCount">この行の出現数を出力するならtrue</param>
        /// <param name="count">この行の出現回数</param>
        /// <param name="formatString">outputCountにtrueを指定した時に使用するフォーマット文字列</param>
        /// <returns></returns>
        private Line CreateLine(
            long lineNumber,
            string text,
            bool outputCount,
            long count,
            string formatString)
        {
            return new Line(
                lineNumber,
                (outputCount ? string.Format(formatString, count) + " " + text : text));
        }

        /// <summary>
        /// 重複判定のためのキー文字列を作る
        /// </summary>
        /// <param name="source">入力となる行文字列</param>
        /// <param name="fieldCount">デリミタで分割されたフィールドを判定対象とする場合は0から始まるインデックス. フィールドを使用しない場合は負の値</param>
        /// <param name="delimiter">fieldCountをtrueに設定した場合のデリミタ文字</param>
        /// <param name="charaCount">先頭から指定した文字数を判定対象とする場合、その文字数. 文字数を判定に使用しない場合は負の値.</param>
        /// <returns>重複判定に使用するキー文字列.</returns>
        private string CreateKey(string source, int fieldCount, char[] delimiter, int charaCount)
        {
            if (0 <= fieldCount)
            {
                var values = source.Split(delimiter);
                return (fieldCount < values.Length ? values[fieldCount] : "");
            }
            else if (0 < charaCount)
            { return source.Substring(0, charaCount); }
            else
            { return source; }
        }

    }
}
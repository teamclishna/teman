﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using clUtils.util;

using TeMan.model;
using TeMan.Properties;
using TeMan.utils;

namespace TeMan.commands.manco
{
    /// <summary>
    /// スナップショットに指定したスナップショット、またはファイルを挿入する
    /// </summary>
    public class AppendCommand : IManipulationCommand
    {
        public string CommandName => "append";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[] {
            new CommandLineOptionDefinition() { LongSwitch = "snapshot", RequireParam = true, },
            new CommandLineOptionDefinition() { LongSwitch = "file", RequireParam = true, },
            new CommandLineOptionDefinition() { LongSwitch = "encoding", RequireParam = true, },
            new CommandLineOptionDefinition() { LongSwitch = "before", RequireParam = true, },
        };

        public Snapshot Execute(Snapshot source, CommandLineOptions options, string[] args)
        {
            // スナップショットとファイルはいずれか一方しか指定できない
            if (options.Options.ContainsKey("snapshot") && options.Options.ContainsKey("file"))
            { throw new ArgumentException(Resources.CMD_APPEND_SNAPSHOT_AND_FILE); }

            // 挿入するスナップショット
            Snapshot appendSnapshot = (
                options.Options.ContainsKey("snapshot") ? GetApppendSnapshot(source, options.Options["snapshot"].ElementAt(0)) :
                null);

            // ファイルから挿入する場合のエンコーディング
            var encoding = (
                options.Options.ContainsKey("encoding") ? Encoding.GetEncoding(options.Options["encoding"].ElementAt(0)) :
                !string.IsNullOrWhiteSpace(Properties.Settings.Default.ENCODING) ? Encoding.GetEncoding(Properties.Settings.Default.ENCODING) :
                null);

            // 挿入するファイル
            var appendFile = (
                options.Options.ContainsKey("file") ? new FileInfo(options.Options["file"].ElementAt(0)) :
                null);

            // 挿入位置
            long insertBefore = 0;
            if (options.Options.ContainsKey("before"))
            {
                if (!long.TryParse(options.Options["before"].ElementAt(0), out insertBefore) || insertBefore < 0)
                {
                    // 行番号の書式が不正
                    throw new ArgumentException(Resources.CMD_APPEND_INVALID_LINE_NUMBER);
                }
                else if (source.FindLine(insertBefore) < 0)
                {
                    // 指定した行番号が見つからない
                    throw new ArgumentException(string.Format(Resources.CMD_APPEND_LINE_NOT_FOUND, insertBefore));
                }
            }

            // スナップショット、ファイルとも指定されていない場合はダイアログから選択
            // ここでキャンセルした場合は空のスナップショットを返して終了
            if (!options.Options.ContainsKey("snapshot") && !options.Options.ContainsKey("file"))
            {
                var selectedFilePath = OpenFiles();
                if (selectedFilePath == null)
                {
                    return new Snapshot()
                    {
                        Name = null,
                        Parent = source,
                    };
                }
                else
                { appendFile = new FileInfo(selectedFilePath); }
            }

            // ファイルからスナップショットを取得
            if(appendFile != null)
            {
                var loadedFile = (encoding == null ? appendFile.Load() : appendFile.Load(encoding));
                appendSnapshot = loadedFile.Snapshots.ElementAt(0);
            }

            var result = new Snapshot()
            {
                Name = null,
                Parent = source
            };

            // 行番号は新しく振りなおす
            // 挿入位置が指定されている場合はその直前行に読み込んだスナップショットを挿入
            var currentLine = 1L;
            foreach (var line in source.Lines)
            {
                if (0 < insertBefore && line.LineNumber == insertBefore)
                {
                    foreach (var appendLine in appendSnapshot.Lines)
                    { result.Lines.Add(new Line(currentLine++, appendLine.Text)); }
                }
                result.Lines.Add(new Line(currentLine++, line.Text));
            }

            // 挿入位置未指定の場合は末尾に挿入
            if (insertBefore == 0)
            {
                foreach (var appendLine in appendSnapshot.Lines)
                { result.Lines.Add(new Line(currentLine++, appendLine.Text)); }
            }

            return result;
        }

        /// <summary>
        /// 挿入対象のスナップショットを取得する
        /// </summary>
        /// <param name="source"></param>
        /// <param name="snapshotName">スナップショット名</param>
        /// <returns>snapshotName で指定したスナップショット. 該当のスナップショットが見つからなかった場合は ArgumentException がスローされる</returns>
        private Snapshot GetApppendSnapshot(Snapshot source, string snapshotName)
        {
            var insertSnapshot = source.FindSnapshot(string.IsNullOrWhiteSpace(snapshotName) ? null : snapshotName);
            if (insertSnapshot == null)
            { throw new ArgumentException(string.Format(Resources.CMD_APPEND_INVALID_SNAPSHOT, snapshotName)); }
            else
            { return insertSnapshot; }
        }

        /// <summary>
        /// ダイアログでファイルを選択する
        /// </summary>
        /// <returns>開いたファイルのフルパス. ダイアログをキャンセルした場合はnull.</returns>
        private string OpenFiles()
        {
            var dlg = new Microsoft.Win32.OpenFileDialog()
            {
                Filter = Resources.DLG_FILTER_FILEOPEN,
                CheckFileExists = true,
                Multiselect = false,
            };

            if (dlg.ShowDialog() == true)
            { return dlg.FileName; }
            else
            { return null; }
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using clUtils.util;
using TeMan.model;

namespace TeMan.commands
{
    /// <summary>
    /// スナップショットの行番号を振りなおすコマンド
    /// </summary>
    public class RenumCommand : IManipulationCommand
    {
        public string CommandName => "renum";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[]
        {
            new CommandLineOptionDefinition() { LongSwitch = "start", RequireParam = true, },
            new CommandLineOptionDefinition() { LongSwitch = "step", RequireParam = true, },
        };

        public Snapshot Execute(Snapshot source, CommandLineOptions options, string[] args)
        {
            // 新しい行番号の開始位置と増分数
            var startNum = (options.Options.ContainsKey("start") ? int.Parse(options.Options["start"].ElementAt(0)) : 1);
            var step = (options.Options.ContainsKey("step") ? int.Parse(options.Options["step"].ElementAt(0)) : 1);

            var result = new Snapshot()
            {
                Name = null,
                Parent = source
            };
            var currentLine = startNum;
            foreach (var line in source.Lines)
            {
                result.Lines.Add(new Line(currentLine, line.Text, line.Highlights));
                currentLine += step;
            }

            return result;
        }
    }
}

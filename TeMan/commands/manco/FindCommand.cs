﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TeMan.model;

using clUtils.util;

namespace TeMan.commands
{
    /// <summary>
    /// 指定した文字列を含む行を返す
    /// </summary>
    public class FindCommand : IManipulationCommand
    {
        public string CommandName => "find";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[] {
            new CommandLineOptionDefinition() { LongSwitch = "regex", RequireParam = false, },
            new CommandLineOptionDefinition() { LongSwitch = "nocase", RequireParam = false, },
        };

        public Snapshot Execute(Snapshot source, CommandLineOptions options, string[] args)
        {
            // 正規表現を使用？
            var useRegEx = options.Options.ContainsKey("regex");
            // 大文字小文字を区別しない？
            var noCase = options.Options.ContainsKey("nocase");

            // 検索語が指定されている?
            if (options.Args.Count() < 1)
            { throw new Exception(Properties.Resources.CMD_FIND_NO_PARAM); }

            var result = new Snapshot()
            {
                Name = null,
                Parent = source
            };
            var keyword = options.Args.ElementAt(0);

            foreach (var line in source.Lines)
            {
                if (Find(line.Text, keyword, useRegEx, noCase))
                {
                    result.Lines.Add(
                        new Line(
                            line.LineNumber,
                            line.Text,
                            CreateHighlights(line.Text, keyword, useRegEx, noCase)));
                }
            }

            return result;
        }

        /// <summary>
        /// 文字列中の指定したキーワードを検索してそのインデックスを返す
        /// </summary>
        /// <param name="source">検索を行う行</param>
        /// <param name="keyword">検索対象のキーワード文字列</param>
        /// <param name="useRegEx">検索キーワードが正規表現かどうか</param>
        /// <param name="noCase">検索キーワードが大文字小文字を区別するかどうか. useRegEx が false の時有効.</param>
        /// <returns>行中に検索キーワードが含まれる場合はtrue.</returns>
        private bool Find(
            string source,
            string keyword,
            bool useRegEx,
            bool noCase)
        {
            return (
                useRegEx ? Regex.IsMatch(source, keyword) :
                0 <= source.IndexOf(keyword, (noCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal))
                );
        }

        /// <summary>
        /// 検索対象の行とキーワードをもとにハイライト表示を行う.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="keyword">検索対象のキーワード文字列</param>
        /// <param name="useRegEx">検索キーワードが正規表現かどうか</param>
        /// <param name="noCase">検索キーワードが大文字小文字を区別するかどうか. useRegEx が false の時有効.</param>
        /// <returns></returns>
        private IEnumerable<Highlight> CreateHighlights(
            string text,
            string keyword,
            bool useRegEx,
            bool noCase)
        {
            var result = new List<Highlight>();
            if (useRegEx)
            {
                var current = 0;
                var mc = Regex.Matches(text, keyword);
                foreach (Match m in mc)
                {
                    if (current < m.Index)
                    { result.Add(new Highlight(text.Substring(current, m.Index - current), HighlightType.Normal)); }

                    result.Add(new Highlight(m.Value, HighlightType.Highlight));
                    current = m.Index + m.Value.Length;
                }

                if (current < text.Length - 1)
                { result.Add(new Highlight(text.Substring(current), HighlightType.Normal)); }

                return result;
            }
            else
            {
                var current = 0;
                while (true)
                {
                    var find = text.IndexOf(keyword, current, (noCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal));
                    if (find < 0)
                    {
                        result.Add(new Highlight(text.Substring(current), HighlightType.Normal));
                        return result;
                    }
                    else
                    {
                        result.Add(new Highlight(text.Substring(current, find - current), HighlightType.Normal));
                        result.Add(new Highlight(text.Substring(find, keyword.Length), HighlightType.Highlight));
                        current = find + keyword.Length;
                    }
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using clUtils.util;

using TeMan.model;
using TeMan.utils;

namespace TeMan.commands.manco
{
    /// <summary>
    /// 行を折り返すコマンド
    /// </summary>
    class WrapCommand : IManipulationCommand
    {
        public string CommandName => "wrap";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[] {
            new CommandLineOptionDefinition() { LongSwitch = "zenkaku", RequireParam = false, },
        };

        public Snapshot Execute(Snapshot source, CommandLineOptions options, string[] args)
        {
            // 全角文字を2文字として扱う?
            var useZenkaku = options.Options.ContainsKey("zenkaku");

            // 分割文字数
            if (options.Args.Count() < 1)
            { throw new ArgumentException(Properties.Resources.CMD_WRAP_NO_PARAM); }
            int wrapCount = 0;
            if(!int.TryParse(options.Args.ElementAt(0), out wrapCount) || wrapCount <=0)
            { throw new ArgumentException(Properties.Resources.CMD_WRAP_NO_PARAM); }

            var result = new Snapshot()
            {
                Name = null,
                Parent = source
            };

            var currentLineNumber = 1L;
            foreach (var line in source.Lines)
            {
                var wrapLines = Wrap(line.Text.ToCharArray(), useZenkaku, wrapCount,currentLineNumber);
                currentLineNumber += wrapLines.Count();

                foreach(var wl in wrapLines)
                { result.Lines.Add(wl); }
            }

            return result;
        }

        /// <summary>
        /// 行の折り返しを行う
        /// </summary>
        /// <param name="chars">行文字列の配列</param>
        /// <param name="useZenkaku">全角文字を2文字として扱う場合はtrue</param>
        /// <param name="wrapLength">折り返し文字数</param>
        /// <param name="startLineNumber">行番号の開始番号</param>
        /// <returns>wrapLengthを超えない最大の文字数で折り返した行データ</returns>
        private IEnumerable<Line> Wrap(char[] chars, bool useZenkaku, int wrapLength,long startLineNumber)
        {
            List<Line> result = new List<Line>();
            var currentLineNumber = startLineNumber;
            var sb = new StringBuilder();
            var charCnt = 0;
            foreach (var c in chars)
            {
                var l = c.CharacterWidth(useZenkaku);
                if (wrapLength < charCnt + l)
                {
                    result.Add(new Line(currentLineNumber++, sb.ToString()));
                    sb.Clear();
                    charCnt = 0;
                }

                sb.Append(c);
                charCnt += l;
            }

            // 残っている文字列を行に追加する
            result.Add(new Line(currentLineNumber, sb.ToString()));

            return result;
        }
    }
}

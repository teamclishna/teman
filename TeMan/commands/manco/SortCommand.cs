﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using clUtils.util;
using TeMan.model;

namespace TeMan.commands
{
    /// <summary>
    /// 行をソートするためのコマンド
    /// </summary>
    public class SortCommand : IManipulationCommand
    {
        public string CommandName => "sort";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[]
        {
            new CommandLineOptionDefinition() { LongSwitch = "reverse", RequireParam = false, AllowMultiple = false, ShortSwitch = 'r' },
            new CommandLineOptionDefinition() { LongSwitch = "field", RequireParam = true, AllowMultiple = false, },
            new CommandLineOptionDefinition() { LongSwitch = "delimiter", RequireParam = true, AllowMultiple = false, },
            new CommandLineOptionDefinition() { LongSwitch = "trim", RequireParam = false, AllowMultiple = false, ShortSwitch = 't' },
        };

        public Snapshot Execute(Snapshot source, CommandLineOptions options, string[] args)
        {
            // 逆順ソート
            var sortDesc = options.Options.ContainsKey("reverse");
            // ソート時に先頭の空白を無視する
            var trim = options.Options.ContainsKey("trim");

            // 指定したフィールドをキーとしてソートする
            // フィールドの区切り文字は delimiter スイッチのパラメータを使用し、 delimiter が指定されていない場合は空白を区切り文字とする
            var fieldCount = (options.Options.ContainsKey("field") ? int.Parse(options.Options["field"].ElementAt(0)) : -1);
            var delimiter = (options.Options.ContainsKey("delimiter") ? options.Options["delimiter"].ElementAt(0).ToCharArray() : new char[] { ' ' });

            Func<Line, string> sortFunc = (src) => { return CreateKey(src.Text, trim, fieldCount, delimiter); };
            var sorted = (sortDesc ? source.Lines.OrderByDescending(sortFunc) : source.Lines.OrderBy(sortFunc));

            var result = new Snapshot()
            {
                Name = null,
                Parent = source
            };
            foreach (var line in sorted)
            { result.Lines.Add(line); }

            return result;
        }

        /// <summary>
        /// ソートのためのキー文字列を作る
        /// </summary>
        /// <param name="source">入力となる行文字列</param>
        /// <param name="trim"></param>
        /// <param name="fieldCount">デリミタで分割されたフィールドをキーとする場合はその0から始まるインデックス. フィールドを使用しない場合は負の値.</param>
        /// <param name="delimiter">fieldCountをtrueに設定した場合のデリミタ文字</param>
        /// <returns>ソートキーに使用する文字列.</returns>
        private string CreateKey(string source, bool trim, int fieldCount, char[] delimiter)
        {
            if (0 <= fieldCount)
            {
                var values = source.Split(delimiter);
                return (fieldCount < values.Length ? values[fieldCount] : "");
            }
            else
            { return (trim ? source.Trim() : source); }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TeMan.model;

using clUtils.util;

namespace TeMan.commands
{
    /// <summary>
    /// 文字列置換を行った結果を返す.
    /// </summary>
    public class ReplaceCommand : IManipulationCommand
    {
        public string CommandName => "replace";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[] {
            new CommandLineOptionDefinition() { LongSwitch = "regex", RequireParam = false, },
            new CommandLineOptionDefinition() { LongSwitch = "nocase", RequireParam = false, },
        };

        public Snapshot Execute(Snapshot source, CommandLineOptions options, string[] args)
        {
            // 正規表現を使用？
            var useRegEx = options.Options.ContainsKey("regex");
            // 大文字小文字を区別しない？
            var noCase = options.Options.ContainsKey("nocase");

            // 置換前後のキーワードが指定されていない
            if (options.Args.Count() < 2)
            { throw new Exception(Properties.Resources.CMD_REPLACE_NO_PARAM); }

            var keyword = options.Args.ElementAt(0);
            var replaceTo = options.Args.ElementAt(1);

            var result = new Snapshot()
            {
                Name = null,
                Parent = source
            };

            Regex re = (useRegEx ? new Regex(keyword) : null);
            foreach (var line in source.Lines)
            {
                if (useRegEx)
                { result.Lines.Add(ReplaceRegEx(line, re, replaceTo)); }
                else
                { result.Lines.Add(Replace(line, keyword, replaceTo, noCase)); }
            }

            return result;
        }

        /// <summary>
        /// 指定した行の指定した文字列を置換する
        /// </summary>
        /// <param name="input">入力となる行</param>
        /// <param name="keyword">置換対象の検索語</param>
        /// <param name="replaceTo">置換する文字列</param>
        /// <param name="noCase">大文字小文字を無視する場合はtrue</param>
        /// <returns>置換後の行オブジェクト</returns>
        private static Line Replace(Line input, string keyword, string replaceTo, bool noCase)
        {
            var current = 0;
            var findPos = -1;
            StringBuilder sb = new StringBuilder();
            var highlights = new List<Highlight>();

            findPos = input.Text.IndexOf(
                keyword,
                current,
                (noCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal));
            while (0 <= findPos)
            {
                sb.Append(input.Text.Substring(current, findPos - current));
                sb.Append(replaceTo);

                highlights.Add(new Highlight(input.Text.Substring(current, findPos - current), HighlightType.Normal));
                highlights.Add(new Highlight(replaceTo, HighlightType.Highlight));

                current = findPos + keyword.Length;
                findPos = input.Text.IndexOf(
                    keyword,
                    current,
                    (noCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal));
            }

            if (current < input.Text.Length)
            {
                sb.Append(input.Text.Substring(current));
                highlights.Add(new Highlight(input.Text.Substring(current), HighlightType.Normal));
            }

            return new Line(input.LineNumber, sb.ToString(), highlights);
        }

        /// <summary>
        /// 指定した行の文字列を正規表現パターンに従って置換する
        /// </summary>
        /// <param name="input">入力となる行</param>
        /// <param name="ptn"></param>
        /// <param name="replaceTo">置換する文字列</param>
        /// <returns>置換後の行オブジェクト</returns>
        private static Line ReplaceRegEx(Line input, Regex ptn, string replaceTo)
        {
            var replacedString = ptn.Replace(input.Text, replaceTo);
            var current = 0;
            var highlights = new List<Highlight>();
            var mc = ptn.Matches(input.Text);

            foreach (Match m in mc)
            {
                if (current < m.Index)
                { highlights.Add(new Highlight(input.Text.Substring(current, m.Index - current), HighlightType.Normal)); }

                highlights.Add(new Highlight(replaceTo, HighlightType.Highlight));
                current = m.Index + replaceTo.Length;
            }

            if (current < input.Text.Length)
            { highlights.Add(new Highlight(input.Text.Substring(current), HighlightType.Normal)); }

            return new Line(input.LineNumber, replacedString, highlights);
        }
    }
}

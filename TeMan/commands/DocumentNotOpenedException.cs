﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeMan.commands
{
    /// <summary>
    /// ファイルが開かれていない時に編集コマンドを実行しようとしたときにスローされる例外
    /// </summary>
    public class DocumentNotOpenedException : Exception
    {
    }
}

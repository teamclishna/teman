﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using clUtils.util;

using TeMan.model;
using TeMan.vm;

namespace TeMan.commands
{
    /// <summary>
    /// 指定したコマンドの種類を返すための列挙子
    /// </summary>
    public enum CommandType
    {
        MANIPULATION_COMMAND,
        META_COMMAND,
        NOT_IMPL,
    }

    /// <summary>
    /// コマンドをロードして保持しておくコントローラオブジェクト
    /// </summary>
    public class CommandController
    {
        /// <summary>
        /// アプリケーション操作コマンドの実装を格納した辞書
        /// </summary>
        private Dictionary<string, IMetaCommand> _mecos;

        /// <summary>
        /// コマンドの実装を格納した辞書
        /// </summary>
        private Dictionary<string, IManipulationCommand> _mancos;

        /// <summary>
        /// 初期化
        /// </summary>
        /// <returns>初期化完了したこのオブジェクトを返す.</returns>
        public CommandController Init()
        {
            // コマンドのロード
            // このアセンブリのすべてのクラスを列挙し、
            // コマンドのインターフェイスを実装しているクラスのインスタンスを辞書に格納する
            var asm = Assembly.GetExecutingAssembly();
            this._mecos = LoadMecos(asm);
            this._mancos = LoadMancos(asm);

            return this;
        }

        /// <summary>
        /// アプリケーションのコマンドをロードする.
        /// </summary>
        /// <param name="asm">このアプリケーションのアセンブリ</param>
        /// <returns>生成したコマンドのインスタンスを格納した辞書.</returns>
        private Dictionary<string, IMetaCommand> LoadMecos(Assembly asm)
        {
            var result = new Dictionary<string, IMetaCommand>();

            foreach (var t in asm.GetTypes())
            {
                if (t.GetInterfaces().Any(x => x.Equals(typeof(IMetaCommand))))
                {
                    var cons = t.GetConstructor(Type.EmptyTypes);
                    var instance = cons.Invoke(null) as IMetaCommand;

                    result.Add(instance.CommandName, instance);
                }
            }

            return result;
        }

        /// <summary>
        /// 編集コマンドをロードする.
        /// </summary>
        /// <param name="asm">このアプリケーションのアセンブリ</param>
        /// <returns>生成した編集コマンドのインスタンスを格納した辞書.</returns>
        private Dictionary<string, IManipulationCommand> LoadMancos(Assembly asm)
        {
            var result = new Dictionary<string, IManipulationCommand>();

            foreach (var t in asm.GetTypes())
            {
                if (t.GetInterfaces().Any(x => x.Equals(typeof(IManipulationCommand))))
                {
                    var cons = t.GetConstructor(Type.EmptyTypes);
                    var instance = cons.Invoke(null) as IManipulationCommand;

                    result.Add(instance.CommandName, instance);
                }
            }

            return result;
        }

        /// <summary>
        /// アプリケーション制御コマンドを実行する
        /// </summary>
        /// <param name="vm">アプリケーションのViewModel</param>
        /// <param name="command">実行するコマンド名</param>
        /// <param name="args">コマンドの引数</param>
        public void ExecuteMetaCommand(MainWindowViewModel vm, string command, string[] args)
        {
            var cmdName = command.ToLower();

            // コマンド名に該当するコマンドの実装が存在しなければエラー
            if (!this._mecos.ContainsKey(cmdName))
            { throw new CommandNotFoundException(cmdName); }

            var cmdImpl = this._mecos[cmdName];

            // コマンドライン引数の妥当性を確認する.
            // 必要なパラメータが足りなければここで終了.
            var options = CommandLineUtils.Parse(args, cmdImpl.ArgsSpec);

            // コマンド実行
            cmdImpl.Execute(vm, options, args);
        }

        /// <summary>
        /// コマンドを実行する
        /// </summary>
        /// <param name="source">コマンドの処理対象とする行</param>
        /// <param name="command">実行するコマンド名</param>
        /// <param name="args">コマンドの引数</param>
        /// <returns>
        /// コマンドの実行結果を格納するスナップショット.
        /// スナップショットは source を親として持つ.
        /// 出力を返さないタイプのコマンドの場合、source をそのまま返す.
        /// </returns>
        public Snapshot ExecuteManipulationCommand(Snapshot source, string command, string[] args)
        {
            var cmdName = command.ToLower();

            // コマンド名に該当するコマンドの実装が存在しなければエラー
            if (!this._mancos.ContainsKey(cmdName))
            { throw new CommandNotFoundException(cmdName); }

            // ドキュメントが開かれていない場合もエラー
            if (source == null)
            { throw new DocumentNotOpenedException(); }

            var cmdImpl = this._mancos[cmdName];

            // コマンドライン引数の妥当性を確認する.
            // 必要なパラメータが足りなければここで終了.
            var options = CommandLineUtils.Parse(args, cmdImpl.ArgsSpec);

            // コマンド実行
            return cmdImpl.Execute(source, options, args);
        }

        /// <summary>
        /// 指定した名前のコマンドの種類を返す
        /// </summary>
        /// <param name="commandName">コマンド名</param>
        /// <returns>コマンドの種類</returns>
        public CommandType GetCommandType(string commandName)
        {
            var cmdName = commandName.ToLower();
            return (
                this._mecos.ContainsKey(cmdName) ? CommandType.META_COMMAND :
                this._mancos.ContainsKey(cmdName) ? CommandType.MANIPULATION_COMMAND :
                CommandType.NOT_IMPL);
        }

        public CommandController()
        { }
    }
}

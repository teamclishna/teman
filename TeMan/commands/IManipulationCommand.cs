﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.util;

using TeMan.model;

namespace TeMan.commands
{
    /// <summary>
    /// テキストの編集を行うためのコマンドのインターフェイス
    /// </summary>
    public interface IManipulationCommand
    {
        /// <summary>
        /// このコマンドを呼び出すためのコマンド名
        /// </summary>
        string CommandName { get; }

        /// <summary>
        /// このコマンドが要求するコマンドライン引数の仕様
        /// </summary>
        CommandLineOptionDefinition[] ArgsSpec { get; }

        /// <summary>
        /// コマンドの実行
        /// </summary>
        /// <param name="source">入力となるスナップショット</param>
        /// <param name="options">コマンドラインパーサで解析済みの引数</param>
        /// <param name="args">解析前の文字列配列型のコマンド引数</param>
        /// <returns>
        /// コマンドの実行結果を格納するスナップショット.
        /// スナップショットは source を親として持つ.
        /// 出力を返さないタイプのコマンドの場合、source をそのまま返す.
        /// </returns>
        Snapshot Execute(
            Snapshot source,
            CommandLineOptions options,
            string[] args);
    }
}

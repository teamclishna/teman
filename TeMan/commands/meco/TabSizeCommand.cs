﻿using System;

using clUtils.util;

using TeMan.Properties;
using TeMan.vm;

namespace TeMan.commands.meco
{
    /// <summary>
    /// タブサイズを設定するためのコマンド
    /// </summary>
    public class TabSizeCommand : IMetaCommand
    {
        /// <summary>デフォルトのタブサイズ</summary>
        private static readonly int DEFAULT_TAB_SIZE = 4;

        /// <summary>タブサイズの最大値</summary>
        private static readonly int MAX_TAB_SIZE = 16;

        public string CommandName => "tabsize";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[] {
            new CommandLineOptionDefinition() { LongSwitch = "current", RequireParam = false, },
        };

        public void Execute(MainWindowViewModel vm, CommandLineOptions options, string[] args)
        {
            var settings = Properties.Settings.Default;

            if (args.Length == 0)
            {
                // 引数を指定していないときはデフォルト値に戻す
                vm.TabSize = DEFAULT_TAB_SIZE;
                vm.NotifyMessage = Resources.MSG_SET_TABSIZE_DEFAULT;
                settings.TAB_SIZE = DEFAULT_TAB_SIZE;
                settings.Save();
                return;
            }
            if (options.Options.ContainsKey("current"))
            {
                // 現在のタブサイズを表示
                vm.NotifyMessage = string.Format(Resources.MSG_GET_TABSIZE, settings.TAB_SIZE);
                return;
            }

            // タブサイズを設定
            try
            {
                var newTabSize = int.Parse(args[0]);
                if (0 < newTabSize && newTabSize <= MAX_TAB_SIZE)
                {
                    vm.TabSize = newTabSize;
                    vm.NotifyMessage = string.Format(Resources.MSG_SET_TABSIZE_SUCCESS, newTabSize);
                    settings.TAB_SIZE = newTabSize;
                    settings.Save();
                }
                else
                { vm.NotifyMessage = Resources.MSG_SET_TABSIZE_FAILED; }
            }
            catch (Exception)
            { vm.NotifyMessage = Resources.MSG_SET_TABSIZE_FAILED; }
        }
    }
}

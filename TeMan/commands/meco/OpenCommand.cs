﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using clUtils.util;

using TeMan.commands;
using TeMan.Properties;
using TeMan.utils;
using TeMan.vm;

namespace TeMan.commands.meco
{
    /// <summary>
    /// ファイルを開くコマンド
    /// </summary>
    public class OpenCommand : IMetaCommand
    {
        public string CommandName => "open";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[] {
            new CommandLineOptionDefinition() { LongSwitch = "encoding", RequireParam = true, },
        };

        public void Execute(MainWindowViewModel vm, CommandLineOptions options, string[] args)
        {
            Encoding openEncoding = null;
            // エンコーディング指定されている場合はこのエンコーディングで開く
            // エンコーディング名の指定に失敗した場合はここで終了
            if (options.Options.ContainsKey("encoding"))
            {
                openEncoding = ParseEncoding(vm, options.Options["encoding"].ElementAt(0));
                if (openEncoding == null)
                { return; }
            }
            else
            { openEncoding = vm.DefaultEncoding; }

            // ファイル名が指定されていない場合はファイル名をダイアログで確認する
            string[] fileNames;
            if (0 < options.Args.Count())
            { fileNames = options.Args.ToArray(); }
            else
            {
                fileNames = OpenFiles();
                if (fileNames.Length == 0)
                { return; }
            }

            // ファイルの2つ目以降は別アプリとして開く
            // 既にドキュメントを開いている場合は1つ目も別アプリとする
            int skipCount = 0;
            if(vm.CurrentDocument == null)
            {
                var fi = new System.IO.FileInfo(fileNames[0]);
                var doc = (
                    openEncoding == null ? fi.Load() :
                    fi.Load(openEncoding));

                vm.CurrentDocument = doc;
                vm.NotifyMessage = string.Format(
                    Resources.MSG_FILE_OPEN_SUCCEES,
                    fi.Name,
                    doc.FileEncoding.WebName);
                skipCount = 1;
            }

            if (skipCount < fileNames.Length)
            {
                var exePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                foreach (var filePath in fileNames.Skip(skipCount))
                {
                    var argsLine = string.Format(
                        "\"{0} \" {1}",
                        filePath,
                        (openEncoding != null ? string.Format("--encoding {0}", openEncoding.WebName) : ""));
                    System.Diagnostics.Process.Start(exePath, argsLine);
                }
            }
        }

        /// <summary>
        /// 指定したエンコーディングのパースを試みる
        /// </summary>
        /// <param name="vm"></param>
        /// <param name="encodingName"></param>
        /// <returns></returns>
        private Encoding ParseEncoding(MainWindowViewModel vm, string encodingName)
        {
            try
            { return encodingName.ToEncoding(); }
            catch (Exception)
            {
                vm.NotifyMessage = string.Format(Resources.MSG_SET_ENCODING_FAILED, encodingName);
                return null;
            }
        }

        /// <summary>
        /// ダイアログでファイルを選択する
        /// </summary>
        /// <returns>開いたファイルのフルパス. ダイアログをキャンセルした場合は空の配列.</returns>
        private string[] OpenFiles()
        {
            var dlg = new Microsoft.Win32.OpenFileDialog()
            {
                Filter = Resources.DLG_FILTER_FILEOPEN,
                CheckFileExists = true,
                Multiselect = true,
            };

            if (dlg.ShowDialog() == true)
            { return dlg.FileNames; }
            else
            { return new string[0]; }
        }
    }
}

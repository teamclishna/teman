﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using clUtils.util;

using TeMan.Properties;
using TeMan.utils;
using TeMan.vm;

namespace TeMan.commands.meco
{
    /// <summary>
    /// 現在編集中のファイルを保存するコマンド
    /// </summary>
    public class SaveCommand : IMetaCommand
    {
        public string CommandName => "save";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[] {
            new CommandLineOptionDefinition() { LongSwitch = "encoding", RequireParam = true, },
        };

        public void Execute(MainWindowViewModel vm, CommandLineOptions options, string[] args)
        {
            // ファイルが開かれていない場合は終了
            if (vm.CurrentDocument == null)
            {
                vm.NotifyMessage = Resources.MSG_FILE_SAVE_NOT_OPENED;
                return;
            }

            Encoding saveEncoding = null;
            // エンコーディング指定されている場合は指定したエンコーディングで保存する
            // エンコーディング名の指定に失敗した場合はここで終了
            // 省略時はドキュメントを開いたときのエンコーディングをそのまま使う
            if (options.Options.ContainsKey("encoding"))
            {
                saveEncoding = ParseEncoding(vm, options.Options["encoding"].ElementAt(0));
                if (saveEncoding == null)
                { return; }
            }
            else
            { saveEncoding = vm.CurrentDocument.FileEncoding; }

            // ファイル名が指定されていない場合はファイル名をダイアログで確認する
            string fileName = (0 < options.Args.Count() ? options.Args.ElementAt(0) : SelectSaveFile());
            if (fileName == null)
            { return; }

            // ファイルを保存する
            FileUtils.Save(
                vm.CurrentDocument,
                vm.CurrentSnapshot,
                new FileInfo(fileName),
                saveEncoding);

            vm.NotifyMessage = string.Format(
                Resources.MSG_FILE_SAVE_SUCCESS,
                fileName,
                saveEncoding.WebName);
        }

        /// <summary>
        /// 指定したエンコーディングのパースを試みる
        /// </summary>
        /// <param name="vm"></param>
        /// <param name="encodingName"></param>
        /// <returns></returns>
        private Encoding ParseEncoding(MainWindowViewModel vm, string encodingName)
        {
            try
            { return encodingName.ToEncoding(); }
            catch (Exception)
            {
                vm.NotifyMessage = string.Format(Resources.MSG_SET_ENCODING_FAILED, encodingName);
                return null;
            }
        }

        /// <summary>
        /// ダイアログでファイルを選択する
        /// </summary>
        /// <returns>開いたファイルのフルパス. ダイアログをキャンセルした場合は空の配列.</returns>
        private string SelectSaveFile()
        {
            var dlg = new Microsoft.Win32.SaveFileDialog()
            {
                Filter = Resources.DLG_FILTER_FILESAVE,
                CheckFileExists = true,
            };

            return (dlg.ShowDialog() == true ? dlg.FileName : null);
        }
    }
}

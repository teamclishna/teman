﻿using System;
using System.Text;

using clUtils.util;

using TeMan.Properties;
using TeMan.utils;
using TeMan.vm;

namespace TeMan.commands.meco
{
    /// <summary>
    /// ファイルを開くときのデフォルトエンコーディングを設定するコマンド
    /// </summary>
    public class SetEncodingCommand : IMetaCommand
    {
        public string CommandName => "encoding";

        public CommandLineOptionDefinition[] ArgsSpec => new CommandLineOptionDefinition[] {
            new CommandLineOptionDefinition() { LongSwitch = "auto", RequireParam = false, },
            new CommandLineOptionDefinition() { LongSwitch = "current", RequireParam = false, },
        };

        public void Execute(MainWindowViewModel vm, CommandLineOptions options, string[] args)
        {
            var settings = Properties.Settings.Default;

            if (args.Length == 0)
            {
                // 引数を指定していないときはutf-8に設定する
                vm.DefaultEncoding = new System.Text.UTF8Encoding(false);
                vm.NotifyMessage = Resources.MSG_SET_ENCODING_DEFAULT;
                settings.ENCODING = vm.DefaultEncoding.WebName;
                settings.Save();
                return;
            }
            if (options.Options.ContainsKey("auto"))
            {
                // 日本語自動設定モード
                vm.DefaultEncoding = null;
                vm.NotifyMessage = Resources.MSG_SET_ENCODING_JAPANESE;
                settings.ENCODING = null;
                settings.Save();
                return;
            }
            if (options.Options.ContainsKey("current"))
            {
                // 現在のエンコーディング設定を表示
                vm.NotifyMessage = (
                    vm.DefaultEncoding != null ? string.Format(Resources.MSG_GET_CURRENT_ENCODING, vm.DefaultEncoding.WebName) :
                    Resources.MSG_GET_CURRENT_ENCODING_AUTOMATIC);
                return;
            }

            // 指定した名前をエンコーディング名に解決を試みる
            try
            {
                vm.DefaultEncoding = args[0].ToEncoding();
                vm.NotifyMessage = string.Format(Resources.MSG_SET_ENCODING_SUCCESS, args[0]);
                settings.ENCODING = vm.DefaultEncoding.WebName;
                settings.Save();
            }
            catch (Exception)
            {
                vm.NotifyMessage = string.Format(Resources.MSG_SET_ENCODING_FAILED, args[0]);
            }
        }
    }
}

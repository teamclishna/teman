﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeMan.commands
{
    /// <summary>
    /// 指定したスナップショットが現在のドキュメント中に見つからなかった場合にスローする例外
    /// </summary>
    public class SnapshotNotFoundException : Exception
    {
        public string SnapshotName { get; private set; }

        public SnapshotNotFoundException(String snapshotName) : base()
        { this.SnapshotName = snapshotName; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clUtils.util;

using TeMan.vm;
namespace TeMan.commands
{
    /// <summary>
    /// アプリケーションの設定、操作を行うためのコマンドのインターフェイス
    /// </summary>
    public interface IMetaCommand
    {
        /// <summary>
        /// このコマンドを呼び出すためのコマンド名
        /// </summary>
        string CommandName { get; }

        /// <summary>
        /// このコマンドが要求するコマンドライン引数の仕様
        /// </summary>
        CommandLineOptionDefinition[] ArgsSpec { get; }

        /// <summary>
        /// コマンドの実行
        /// </summary>
        /// <param name="vm">アプリケーションのViewModel</param>
        /// <param name="options">コマンドラインパーサで解析済みの引数</param>
        /// <param name="args">解析前の文字列配列型のコマンド引数</param>
        void Execute(
            MainWindowViewModel vm,
            CommandLineOptions options,
            string[] args);
    }
}

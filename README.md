# TeMan

## これは何? (What's this?)

テキストファイルを開き、コマンドで編集するアプリケーションです。

This application edits text files with commands

このアプリケーションはテキストエディタではありません。
自由に文字を入力して編集する機能はありません。
貴方がそのような機能を必要とするならば優れたテキストエディタが無数に存在します。

This application is not a text editor.
There is no function to enter and edit characters freely.
There are a myriad of good text editors if you need such a feature.

## 必要な動作環境 (System Requirements)

* Windows 10
* .net Framework 4.6.1

## インストール (How to install)

このアプリケーションのインストーラはありません。
以下のファイルを同一のフォルダにおいて Teman.exe を実行します。

This application does not use an installer.
When Teman.exe is executed in the same folder with the following files, the application starts.

* Teman.exe
* clUtils.dll
* clUtils.WPF.dll
* TeMan.chm

## アンインストール (How to uninstall)

このアプリケーションのアンインストーラはありません。
インストール手順で説明した3つのファイルを削除します。

This application does not use an uninstaller.
When deleting an application, delete the files described in the installation procedure.

以下のフォルダにアプリケーションの設定ファイルが保存されます。
設定を残す必要がないのならこのフォルダとサブフォルダを削除します。

If you do not need to leave the application settings, delete this folder and its subfolders.

``%USERPROFILE%\AppData\Local\Team_ClishnA\TeMan.exe_***``

(フォルダ名の末尾の *** の箇所は環境によって異なります。)

(The *** part at the end of the folder name varies depending on the environment.)

## 使い方 (How to use)

アプリケーションを起動したらメニューの「ファイル」→「開く」
またはファイルをドロップするとファイルを開くことができます。

Select "File" "Open" from the application menu, or drop the file from Explorer to open a text file.

![Launched](./readme_images/teman_1.png)

![Open File](./readme_images/teman_2.png)

開いたファイルはコマンドによる編集履歴を示す「スナップショット」という単位で管理します。

画面下部にコマンドを入力するとその結果を示す新しいスナップショットが生成され、
左側のツリービューに表示されます。

コマンドはパイプ記号 「|」 で繋いで一括して実行することができます。

The application manages the history of editing opened files in units called “snapshots”.

When you enter a command at the bottom of the screen,
the result is output as a new snapshot.

![Input manipulation commands](./readme_images/teman_3.png)

![Command executed](./readme_images/teman_4.png)

ツリービューはスナップショットの履歴を示しています。
スナップショットの上の階層を選択すればその時点のスナップショットを表示できます。

The tree view shows the snapshot history.
When you select an item in the middle of a snapshot,
You can display the current text state.

![Previous snapshot](./readme_images/teman_5.png)

そしてその状態でコマンドを入力すれば、そのスナップショットを入力として
新しいスナップショットが作成されます。

If the command is executed while a snapshot in the middle is displayed,
a new snapshot hierarchy is created.

![Execute new command](./readme_images/teman_6.png)

![Command executed](./readme_images/teman_7.png)

## ライセンス (License)

あなたはこのアプリケーションを無償で使用することができます。

このアプリケーションはソースコードを公開しています。
このソースコードをダウンロードしてあなたはバグを修正したり自分に必要な機能を追加することができます。

このアプリケーションはオープンソースソフトウェア**ではありません**。

あなたはこのアプリケーションを配布、転載することはできません。
あなたが修正したソースコードを配布する場合、修正箇所のみを配布することができます。

You can use this application for free.

This application has released the source code.
You can download the source code and fix bugs or add functions.

This application is **not** open source software.

You cannot distribute or reprint this application.
When you distribute the source code that you modified,
you can distribute only the modified part.

## 作者 (Author)

Team ClishnA

* [InfoClishnA](http://clishna.iplus.to/download/teman/)
* [Source code](https://bitbucket.org/teamclishna/teman/)
